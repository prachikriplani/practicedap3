public class feedbackHelper {
    
    @AuraEnabled
    public static List<Account> fetchingAccounts(){
        
        List<Account> accRecords =  [SELECT Id, Name FROM Account];

        return accRecords;
    }

    @AuraEnabled
    public static List<Question__c> fetchingQuestions(){
        
        List<Question__c> questionRecords =  [SELECT Id, Question__c, Answer_Type__c, Available_Options__c, Required__c FROM Question__c];

        return questionRecords;
    }

    @AuraEnabled
    public static void createFeedbackRecords(Object data){
       
        List<Object> answerList = (List<Object>) JSON.deserializeUntyped((String) JSON.serialize(data));
        System.debug(answerList);
        List<Map<String,Object>> finalList = new List<Map<String,Object>>();
        
        for(Object answer : answerList){
            
            Map<String,Object> answerMap = (Map<String,Object>) JSON.deserializeUntyped((String) JSON.serialize(answer));
            finalList.add(answerMap);
        }
        
		System.debug(finalList);
        
        List<Feedback__c> feedbacks = new List<Feedback__c>();
        
        for(Map<String,Object> finalAnswer : finalList){
            
            Id accountId = (Id) finalAnswer.get('account');
            Id questionId = (Id) finalAnswer.get('id');
            Feedback__c feedback = new Feedback__c(Account__c = accountId, Question__c = questionId);
            
            System.debug(finalAnswer.get('answer'));
            
            try{
                
                String answer = (String) finalAnswer.get('answer');
                feedback.Chosen_Answer__c = answer;
                
            }catch(exception e){
                
                System.debug('exception');
                Object answer = (Object) JSON.deserializeUntyped(JSON.serialize(finalAnswer.get('answer')));
                System.debug(answer);
                feedback.Chosen_Answer__c = String.valueOf(answer);
            }
            
            System.debug(feedback);
            feedbacks.add(feedback);
        }
        
        System.debug(feedbacks);
        
        List<Database.SaveResult> results = Database.insert(feedbacks, False);
        
        for(Database.SaveResult result : results){
            
            if(result.isSuccess()){
                
                System.debug(result.getId());
                
            }else{
                
                List<Database.Error> errors = result.getErrors();
                
                for(Database.Error error : errors){
                    
                    System.debug(error.getMessage());
                }
            }
        }
    }
}