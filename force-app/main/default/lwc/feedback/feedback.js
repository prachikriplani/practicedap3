import { LightningElement, api, track } from 'lwc';
import fetchingAccounts from '@salesforce/apex/feedbackHelper.fetchingAccounts';
import fetchingQuestions from '@salesforce/apex/feedbackHelper.fetchingQuestions';
import createFeedbackRecords from '@salesforce/apex/feedbackHelper.createFeedbackRecords';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Feedback extends LightningElement {

    allAccounts = [];
    @track finalData = [];

    connectedCallback(){

        fetchingAccounts().then(result=>{

            this.allAccounts = result;

            console.log("accounts => ", this.allAccounts);
            
        }).catch(error=>{

            console.log("error => ", error);
        })

        fetchingQuestions().then(result=>{

            let allQuestions = [];
            allQuestions = result;

            console.log("allQuestions => ", allQuestions);

            let tempData = [];
            allQuestions.forEach((question)=>{

                let ques = {
                    'question' : question.Question__c,
                    'type' : question.Answer_Type__c,
                    'id': question.Id,
                    'options' : question.Available_Options__c,
                    'required' : question.Required__c,
                    'answer' : '',
                    'account' : this.accountId,
                    'isPicklist' : false,
                    'show' : true,
                    'isCheck': false,
                    'isText' : false,
                    'isRadio': false
                };

                if(question.Answer_Type__c == "Checkbox"){
                    ques['isCheck'] = true;

                    console.log('inside checkbox');
                   
                    let answers = question.Available_Options__c.split(',');
                    console.log("answers => ",JSON.parse(JSON.stringify(answers)));

                    let optionList = [];

                    answers.forEach((value)=>{

                        let answer = {};
                        answer.label = value;
                        answer.value = value;

                        optionList.push(answer);
                    })

                    ques['optionList'] = optionList;
                    console.log(JSON.parse(JSON.stringify(optionList)));
                    
                }else if(question.Answer_Type__c == "Text-Area"){
                    ques['isText'] = true;
                    
                }else if(question.Answer_Type__c == "Picklist"){
                    ques['isPicklist'] = true;
                    console.log('inside picklist');
                   
                    let answers = question.Available_Options__c.split(',');
                    console.log("answers => ",JSON.parse(JSON.stringify(answers)));

                    let optionList = [];

                    answers.forEach((value)=>{

                        let answer = {};
                        answer.label = value;
                        answer.value = value;

                        optionList.push(answer);
                    })

                    ques['optionList'] = optionList;
                    console.log(JSON.parse(JSON.stringify(optionList)));

                }else if(question.Answer_Type__c == "Radio Button"){
                    ques['isRadio'] = true;

                    console.log('inside radio');
                   
                    let answers = question.Available_Options__c.split(',');
                    console.log("answers => ",JSON.parse(JSON.stringify(answers)));

                    let optionList = [];

                    answers.forEach((value)=>{

                        let answer = {};
                        answer.label = value;
                        answer.value = value;

                        optionList.push(answer);
                    })

                    ques['optionList'] = optionList;
                    console.log(JSON.parse(JSON.stringify(optionList)));
                    
                }

                tempData.push(ques);

            })

            this.finalData = JSON.parse(JSON.stringify(tempData));

            console.log("final data => ", JSON.parse(JSON.stringify(this.finalData)));
            
        }).catch(error=>{

            console.log("error => ", error);
        })

    }

    @api accountId = '';

    get optionsAccount() {

        let accountList = [];
        
        this.allAccounts.forEach((account)=>{

            let acc = {};
            acc.label = account.Name;
            acc.value = account.Id;
            accountList.push(acc);
        })
        return accountList;
    }

    getAccountId(event){

        this.accountId = event.detail.value;
        this.finalData.forEach((ques)=>{
            
            ques['account'] = this.accountId;
            
        })
    }

    getAnswer(event){

        let value = '';
        try{
            value = event.target.value;
        }catch(error){
            value = event.detail.value;
        }
       
        let id = event.currentTarget.dataset.id;
        
        this.finalData.forEach((ques)=>{

            if(ques.id == id){

                ques['answer'] = value;
            }
        })
        console.log(JSON.parse(JSON.stringify(this.finalData)));
    }

    submitData(){

        if(this.accountId == ''){

            let myToast = new ShowToastEvent({

                title : 'Required fields missing',
                variant : 'error',
                message : 'Account field is mandatory. Please choose an account'
            });

            this.dispatchEvent(myToast);

        }else{

            this.finalData.forEach((ques)=>{

                if(ques.required == true && ques.answer == ''){

                    let myToast = new ShowToastEvent({

                        title : 'Complete required questions',
                        variant : 'error',
                        message : 'Please answer all the questions which are required (marked with asterik (*)).'
                    });
        
                    this.dispatchEvent(myToast);
                }else{

                    createFeedbackRecords({data:this.finalData}).then((result)=>{

                        console.log("result =>", result);
                    }).catch((error)=>{
            
                        console.log("error => ", error);
                    })
                }
            })

           
        }

    }

    clearData(){

        console.log("data cleared");

    }

    hideElement(event){

        let id = event.currentTarget.dataset.id;

        this.finalData.forEach((ques)=>{

            if(ques.id == id){

                ques['show'] = false;
            }
        })
    }

    showElement(event){

        let id = event.currentTarget.dataset.id;
        console.log(id);
        this.finalData.forEach((ques)=>{

            if(ques.id == id){

                ques['show'] = true;
            }
        })
    }
}